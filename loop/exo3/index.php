<?php

$var1= 100;
$var2= 1;

for ($i=100; $i >= 10; $i--) { 

   echo $i* $var2 . "<br>";
        
    
}
?>
<hr>
<?php
/*##Exercice 4 
Créer une variable et l'initialiser à 1.
Tant que cette variable n'atteint pas 10, il faut :

l'afficher
l'incrementer de la moitié de sa valeur
*/


$x=1;
for ($i=1; $i<10;$i+=$i/2) { 
    echo $i. "<br><br>";
}
?>
<hr>
<?php
/*##Exercice 5
 En allant de 1 à 15 avec un pas de 1,
 afficher le message On y arrive presque */


$y=1;

for ($i=1; $i <=15; $i++) { 
    echo $i . "On y arrive presque" . "<br>";
    
}

/*
##Exercice 6 
En allant de 20 à 0 avec un pas de 1,
 afficher le message C'est presque bon.
*/

$az=20;
for ($i=20; $i >=1 ; $i--) { 
    echo $i . "C'est presque bon". "<br>";
}
?>
<hr>
<?php
/*
##Exercice 7
 En allant de 1 à 100 avec un pas de 15,
  afficher le message On tient le bon bout. */

  $z7= 100;
  for ($i=1; $i <=100 ; $i+=15) { 
      echo $i . "On tient le bon bout"."<br>";
  }

 ?>
<hr>
<?php
/*
##Exercice 8
 En allant de 200 à 0 avec un pas de 12,
  afficher le message Enfin !!!!. */

$dz=200;
for ($i=200; $i >=0 ; $i-=12) { 
    echo $i . "Enfin". "<br>";
}